# RooMate

some person in big city in search for a roommate for division rent of house.
this API can help to build site or app for helping this people.

## using



### 1. first you create profile 

url : /api/v1/profile/ (POST)

    {
    "profile": {
            "user": {
                "username": "<username>",
                "password" : "<password>"
            },
            "first_name": "ali",
            "last_name": "tab",
            "rate": 5.0,
            "number_of_persons_rated": 0
        }
    }



### 2. get profile info

url : /api/v1/profile/ (GET)

    {
        "profile": {
            "id": 4,
            "user": {
                "username": "ali79taba",
                "id": 4
            },
            "first_name": "ali",
            "last_name": "tab",
            "rate": 5.0,
            "number_of_persons_rated": 0,
            "image_package": 4
        }
    }
    
### 3. add profile picture

url : api/v1/profile/images/ (POST)

    {
        "imagefile_set": [
            {
                "url": "<image_file_source_url>",
                "width": 10,
                "height": 10,
                "format_type": 0,          
                "image_size_type": 0,
                "picture_profile": 10
            }
        ]
    }
    
    format type = [(0,'svg'), (1,'jpeg'), (2,'png')]
    image size type = [(0,'small'), (1,'meduim'), (2,'big')]
    
### 4. get profile picture 
    
url : api/v1/profile/images/ (GET)

    [
        {
            "id": 10,
            "imagefile_set": [
                {
                    "id": 6,
                    "url": "hello",
                    "width": 10,
                    "height": 10,
                    "format_type": 0,
                    "image_size_type": 0,
                    "picture_profile": 10
                }
            ],
            "image_package": 4
        },
        {
            "id": 11,
            "imagefile_set": [
                {
                    "id": 7,
                    "url": "hello",
                    "width": 10,
                    "height": 10,
                    "format_type": 0,
                    "image_size_type": 0,
                    "picture_profile": 11
                }
            ],
            "image_package": 4
        }
    ]

### 5. get all request room mate 

url : api/v1/request-room-mates/ (GET)

    [
        {
            "id": 1,
            "request_status": 0,
            "content": "i want seriosly a room mate",
            "longitude": "145.005123",
            "latitude": "999.999999",
            "start_contract_date": "2019-07-16",
            "end_contract_date": "2019-07-20",
            "area": 50,
            "floor": 2,
            "number_of_persons_will_be_in_room": 5,
            "user": 1,
            "image_package": 2
        },
        {
            "id": 2,
            "request_status": 0,
            "content": "dfdksdlfj",
            "longitude": "0.000001",
            "latitude": "0.000001",
            "start_contract_date": "2019-07-21",
            "end_contract_date": "2019-07-21",
            "area": 1,
            "floor": 1,
            "number_of_persons_will_be_in_room": 1,
            "user": 3,
            "image_package": 3
        }
    ]
    
    requset_status = [(0,'actice'), (1,'founded'), (2,'cancled')]
    
### 6. create request room mate

url : api/v1/request-room-mates/ (POST)

    {
        "request_status": <status>,
        "content": "<content>",
        "longitude": <longitude>,
        "latitude": <latitude>,
        "start_contract_date": <start_contract_date>,
        "end_contract_date": <end_contract_date>,
        "area": <area>,
        "floor": <floor>,
        "number_of_persons_will_be_in_room": <number>
    }
    
    
### 7. add picture to request room mate

url : api/v1/request-room-mates/image/ (POST)

    {
        "imagefile_set": [
            {
                "url": "<image_url>",
                "width": <width>,
                "height": <height>,
                "format_type": <format_type>,
                "image_size_type": <image_size_type>
            }
        ]
    }
    
    format type : [(0,'svg'), (1,'jpeg'), (2,'png')]
    image size type : [(0,'small'), (1,'meduim'), (2,'big')]
    
    
### 8. get picture of request room mate

url : api/v1/request-room-mate/<request-room-mate_id>/image/ (GET)

    [
        {
            "id": 12,
            "imagefile_set": [
                {
                    "id": 8,
                    "url": "dsoljdljfl",
                    "width": 1,
                    "height": 1,
                    "format_type": 0,
                    "image_size_type": 0,
                    "picture_profile": 12
                }
            ],
            "image_package": 5
        },
        {
            "id": 13,
            "imagefile_set": [
                {
                    "id": 9,
                    "url": "dsoljdljfl",
                    "width": 1,
                    "height": 1,
                    "format_type": 0,
                    "image_size_type": 0,
                    "picture_profile": 13
                }
            ],
            "image_package": 5
        }
    ]
    
### 9. create a request to add in pending list of request room mate

url : api/v1/pending/ (POST)

    {
        "request_room_mate": <requset-room-mate_id>
    }
    

### 10. get list of my request pending

url : api/v1/pending/ (GET)

    [
        {
            "id": 6,
            "status": 0,
            "user_want_to_join": 4,
            "request_room_mate": 3
        }
    ]
    
### 11. create contract with the persons in pending list

url : api/v1/contract/created (POST)

    {
        "contract_status": 0,
        "request_room_mate": 3,
        "accepted_persons": [4]
    }
    
    contract_status = [(0,"active"), (1,"inactive")]
    
### 12. get list of my created contract list

url : api/v1/contract/created (GET)


    [
        {
            "id": 11,
            "contract_status": 0,
            "requested_person": 4,
            "request_room_mate": 3,
            "accepted_persons": [
                4
            ]
        }
    ]


### 13. list of my accepet contract

url : api/v1/contract/accepted (GET)

    [
        {
            "id": 11,
            "contract_status": 0,
            "requested_person": 4,
            "request_room_mate": 3,
            "accepted_persons": [
                4
            ]
        }
    ]
    
    
### 14. create review

url : api/v1/review/create (POST)

    {
        "rate": 2,
        "content": "dasfdf",
        "receiver_review": 4,
        "request_room_mate": 3
    }
    
### 15. list of received review

url : api/v1/review/receive  (GET)

    [
        {
            "id": 2,
            "rate": 2,
            "content": "dasfdf",
            "date_time": "2019-07-25T12:15:07.560380Z",
            "sender_review": 4,
            "receiver_review": 4,
            "request_room_mate": 3
        }
    ]
    
### 16. list of my sended review

url : api/v1/review/send (GET)

    [
        {
            "id": 2,
            "rate": 2,
            "content": "dasfdf",
            "date_time": "2019-07-25T12:15:07.560380Z",
            "sender_review": 4,
            "receiver_review": 4,
            "request_room_mate": 3
        }
    ]
    
    
### 17. chat with a person 

url : api/v1/chat/create (POST)

    {
        "content": "dsldjdjdlsd",
        "receiver_chat": 4
    }
    
### 18. list of received chat from a person

url : api/v1/chat/receive/<int:from_user_received>/<int:stamp>/ (GET)


    [
        {
            "id": 5,
            "content": "dsldjdjdlsd",
            "send_time": 1564057233,
            "sender_chat": 4,
            "receiver_chat": 4
        }
    ]
    
### 19. list of sended chat to a person

url : api/v1/chat/send/<int:to_user_sended>/<int:stamp>/ (GET)

    [
        {
            "id": 5,
            "content": "dsldjdjdlsd",
            "send_time": 1564057233,
            "sender_chat": 4,
            "receiver_chat": 4
        }
    ]

### 20. list of person that I chat with them

url : api/v1/contact (GET)

    [
        {
            "username": "ali79taba",
            "id": 4
        }
    ]
    
## Authenticating 

for authenticate you must give a token from us with this url :

url : api/v1/token (POST)

    {
        "username": "",
        "password": ""
    }

and you are given token

    {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmlnX2lhdCI6MTU2NDA1Nzc3NiwidXNlcl9pZCI6MSwiZW1haWwiOiIiLCJ1c2VybmFtZSI6ImFsaSIsImV4cCI6MTU2NjY0OTc3Nn0.tdjw5M0wedXe9ACgL2_wgpHkMA1eIyHN3m5C_49ShZc"
    }
    
then you must put the token in header in this foramt :

    HEADER:
    
    Authorization: JWT <YOUR_TOKEN>