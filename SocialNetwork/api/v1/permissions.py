from rest_framework import permissions
from SocialNetwork.models import RequestRoomMate

class IsAcceptedPersonsInPendingState(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method == 'GET':
            return True
        print(request.method)
        accepted_persons = request.data.get('accepted_persons')
        request_id = request.data.get('request_room_mate')
        pending_set = RequestRoomMate.objects.get(pk=request_id).pending_set
        for accepted_person in accepted_persons:
            queryset = pending_set.filter(user_want_to_join=accepted_person)
            if queryset.count == 0:
                return False
            if queryset[0]['status'] == 1:
                return False
            print("************")
        print("^^^^^^^^^^^^^^^^^^")
        return True

