from django.urls import path

from SocialNetwork.api.v1.views import ProfileView, ProfileImageView, RequestRoomMateView
from SocialNetwork.api.v1.views import MyPendingRoomMateView, ContractAPIView, MeAddedToContractAPIView
from SocialNetwork.api.v1.views import ReviewRecievedAPIView, ReviewSendAPIView, ChatAPIView, ChatRecieveAPIView,CreateChatAPIView
from SocialNetwork.api.v1.views import CreateReviewAPIView,ImageRequestAPIView, ContactAPIView
from rest_framework_jwt.views import ObtainJSONWebToken

urlpatterns = [
    path('profile/', ProfileView.as_view()),
    path('profile/images/', ProfileImageView.as_view()),
    path('request-room-mates/', RequestRoomMateView.as_view()),
    path('request-room-mates/<int:request_id>/image', ImageRequestAPIView.as_view()),
    path('pending/', MyPendingRoomMateView.as_view()),
    path('contract/created/', ContractAPIView.as_view()),
    path('contact/', ContactAPIView.as_view()),
    path('contract/accepted/', MeAddedToContractAPIView.as_view()),
    path('review/receive/', ReviewRecievedAPIView.as_view()),
    path('review/send/', ReviewSendAPIView.as_view()),
    path('review/create/', CreateReviewAPIView.as_view()),
    path('chat/create/', CreateChatAPIView.as_view()),
    path('chat/send/<int:to_user_sended>/<int:stamp>/', ChatAPIView.as_view()),
    path('chat/receive/<int:from_user_received>/<int:stamp>/', ChatRecieveAPIView.as_view()),
    path('token/', ObtainJSONWebToken.as_view()),
]