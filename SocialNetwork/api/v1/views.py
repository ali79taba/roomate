from datetime import datetime

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import exceptions, generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import NotFound, APIException

from SocialNetwork.models import Profile, RequestRoomMate, ImagePackage
from SocialNetwork.api.v1.serializers import ProfileSerializer, PictureProfileSerializer, CreateProfileSerializer, RequestRoomMateSerializer
from SocialNetwork.api.v1.serializers import PendingSerializer, CreatePendingSerializer, ContractSerializer, ReviewSerializer, ChatSerializer
from SocialNetwork.api.v1.serializers import CreateChatSerializer, CreateReviewSerializer, CreatePictureProfileSerializer, CreateRequestRoomMateSerializer
from SocialNetwork.api.v1.serializers import CreateContractSerializer, ContactSerializer, UserSerializer
from SocialNetwork.api .v1.permissions import IsAcceptedPersonsInPendingState

from django.core.exceptions import ObjectDoesNotExist

PENDING_CODE = 0

class ProfileView(APIView):

    def get(self, request):
        if hasattr(request, 'user') is False:
            raise exceptions.APIException("you are not loggined")
        if hasattr(request.user, 'profile') is False:
            raise exceptions.APIException("you aren't create profile")
        profile = request.user.profile
        serializer = ProfileSerializer(profile)
        response = {
            'profile' : serializer.data
        }
        return Response(response)

    def post(self, request):
        profile = request.data.get('profile')
        serializer = CreateProfileSerializer(data=profile)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
        success = {
            "success": "profile '{}' created successfully".format(profile)
        }
        return Response(success)
    

class ProfileImageView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated, ]
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return PictureProfileSerializer
        else:
            return CreatePictureProfileSerializer

    def get_queryset(self):
        try:
            return self.request.user.profile.image_package.pictureprofile_set
        except ObjectDoesNotExist:
            return []

    def create(self, request):
        user = request.user
        data = request.data
        serializer = self.get_serializer_class()(data=data)
        if serializer.is_valid():
            image_package = user.profile.image_package
            serializer.save(image_package=image_package)
        return Response(serializer.data)


class RequestRoomMateView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = RequestRoomMateSerializer

    def get_queryset(self):
        try:
            return RequestRoomMate.objects.all()
        except ObjectDoesNotExist:
            return []

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return RequestRoomMateSerializer
        else:
            return CreateRequestRoomMateSerializer
    
    def create(self, request):
        user = request.user
        data = request.data
        serializer = self.get_serializer_class()(data=data)
        if serializer.is_valid():
            image_package = ImagePackage.objects.create()
            serializer.save(user=user, image_package=image_package)
        return Response(serializer.data)

class ImageRequestAPIView(APIView):
    permission_classes = [IsAuthenticated, ]
    def get(self, request, request_id):
        queryset = RequestRoomMate.objects.get(pk=request_id).image_package.pictureprofile_set
        serializer = PictureProfileSerializer(queryset, many=True)
        return Response(serializer.data)
    
    def post(self, request, request_id):
        data = request.data
        serializer = CreatePictureProfileSerializer(data=data)
        image_package = RequestRoomMate.objects.get(pk=request_id).image_package
        if serializer.is_valid(raise_exception=True):
            serializer.save(image_package=image_package)
        return Response(serializer.data)

        
class MyPendingRoomMateView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated,]

    def get_queryset(self):
        return self.request.user.pending_set.all()
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return PendingSerializer
        else:
            return CreatePendingSerializer

    def create(self, request):
        user = request.user
        data = request.data
        serializer = self.get_serializer_class()(data=data)
        if serializer.is_valid(raise_exception=True):
            serializer.save(user_want_to_join=user, status=PENDING_CODE)
        return Response(serializer.data)        

class ContractAPIView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        return self.request.user.contracts_created.all()
    
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return ContractSerializer
        else:
            return CreateContractSerializer

    def check_not_found_pending_user(self, request):
        accepted_persons = request.data.get('accepted_persons')
        request_id = request.data.get('request_room_mate')
        pending_set = RequestRoomMate.objects.get(pk=request_id).pending_set.all()
        print(pending_set)
        for accepted_person in accepted_persons:
            queryset = pending_set.filter(user_want_to_join=accepted_person)
            print(queryset)
            if queryset.count() == 0:
                raise APIException('in your peding person list have a person that not found in data base')
            if queryset[0].status == 1:
                raise APIException('in your peding person list have a person that not found in data base')
    
    def create(self, request):
        self.check_not_found_pending_user(request=request)
        user = request.user
        data = request.data
        serializer = self.get_serializer_class()(data=data)
        if serializer.is_valid(raise_exception=True):
            serializer.save(requested_person=user)
        return Response(serializer.data)

class MeAddedToContractAPIView(generics.ListAPIView):
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        return self.request.user.contracts_accepted.all()
    
    serializer_class = ContractSerializer

class ReviewRecievedAPIView(generics.ListAPIView):
    permission_classes = [IsAuthenticated, ]
    serializer_class = ReviewSerializer

    def get_queryset(self):
        return self.request.user.review_received.all()
    

class ReviewSendAPIView(generics.ListAPIView):
    permission_classes = [IsAuthenticated, ]
    serializer_class = ReviewSerializer

    def get_queryset(self):
        return self.request.user.review_sended.all()

class CreateReviewAPIView(generics.CreateAPIView):
    perimissions_classes = [IsAuthenticated, ]
    serializer_class = CreateReviewSerializer

    def check_the_user_exist_in_contract(self, request):
        request_id = request.data.get('request_room_mate')
        try:
            contract = RequestRoomMate.objects.get(pk=request_id).contract
        except ObjectDoesNotExist:
            raise APIException("contract doesn't found")
        user = contract.accepted_persons.filter(pk=request.user.id)
        if user.count == 0:
            raise APIException("you aren't in contract")

    def check_twice_send_review(self, request):        
        sended_review = request.user.review_sended
        request_id = request.data.get('request_room_mate')
        request_room_mate = RequestRoomMate.objects.get(pk=request_id)
        if sended_review.filter(request_room_mate=request_room_mate).count() > 0:
            raise APIException("you reviewed in this request room mate")
    

    def create(self, request):
        self.check_the_user_exist_in_contract(request=request)
        self.check_twice_send_review(request=request)
        user = request.user
        data = request.data
        serializer = self.serializer_class(data=data)
        if serializer.is_valid():
            now = datetime.now()
            serializer.save(sender_review=user, date_time=now)
            return Response(serializer.data)

class ChatAPIView(APIView):
    permission_classes = [IsAuthenticated, ]     
    def get(self, request, to_user_sended, stamp):
        queryset = request.user.chat_sended
        limited_query_set = queryset.filter(receiver_chat=to_user_sended).filter(send_time__gte=stamp)
        serializer = ChatSerializer(limited_query_set, many=True)
        return Response(serializer.data)
        


class ChatRecieveAPIView(APIView):
    permission_classes = [IsAuthenticated, ]

    def get(self, request, from_user_received, stamp):
        queryset = request.user.chat_received.filter(sender_chat=from_user_received).filter(send_time__gte=stamp)
        serializer = ChatSerializer(queryset, many=True)
        return Response(serializer.data)


class CreateChatAPIView(generics.CreateAPIView):
    permissions_classes = [IsAuthenticated, ]
    serializer_class = CreateChatSerializer

    def create(self, request):
        user = request.user
        data = request.data
        serializer = self.serializer_class(data=data)
        if serializer.is_valid():
            now = datetime.now()
            chat = serializer.save(sender_chat=user, send_time=datetime.timestamp(now))
            receiver = chat.receiver_chat
            user.contact.user.add(receiver)
            user.contact.save()
        return Response(serializer.data)


class ContactAPIView(generics.ListAPIView):
    permission_classes = [IsAuthenticated, ]
    serializer_class = UserSerializer

    def get_queryset(self):
        return self.request.user.contact.user.all()
    
