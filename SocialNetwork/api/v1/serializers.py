from rest_framework import serializers

from SocialNetwork.models import Profile, PictureProfile, ImageFile, ImagePackage, RequestRoomMate
from SocialNetwork.models import Pending, Contract, Review, Chat, Contact
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('username','id')

class CreateUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('username','password')

class ProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    class Meta:
        model = Profile
        fields = '__all__'


class CreateProfileSerializer(serializers.ModelSerializer):
    user = CreateUserSerializer()
    class Meta:
        model = Profile
        fields = ['first_name', 'last_name', 'rate', 'number_of_persons_rated', 'user']

    def create(self, validated_data):
        user_detail = validated_data.pop('user')
        password = user_detail['password']
        user = User.objects.create_user(username=user_detail['username'], password=password)
        user.save()
        validated_data['user'] = user

        contact = Contact.objects.create(owner=user)
        contact.save()

        image_package = ImagePackage.objects.create()
        validated_data['image_package'] = image_package

        profile = Profile(**validated_data)
        profile.save()
        return profile

class ImageFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImageFile
        fields = '__all__'

class CreateImageFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImageFile
        exclude = ('picture_profile',)


class PictureProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = PictureProfile
        fields = '__all__'
    imagefile_set =  ImageFileSerializer(many=True)


class CreatePictureProfileSerializer(serializers.ModelSerializer):
    imagefile_set = CreateImageFileSerializer(many=True)

    class Meta:
        model = PictureProfile
        exclude = ('image_package',)

    def create(self, validated_data):
        imagefile_set = validated_data.pop('imagefile_set')
        picture_profile = PictureProfile.objects.create(**validated_data)
        for image_file in imagefile_set:
            image_file_serializer = CreateImageFileSerializer(data=image_file)
            if image_file_serializer.is_valid(raise_exception=True):
                image_file_serializer.save(picture_profile=picture_profile)
        return picture_profile


         
    

class RequestRoomMateSerializer(serializers.ModelSerializer):
    class Meta:
        model = RequestRoomMate
        fields = '__all__'
    
class CreateRequestRoomMateSerializer(serializers.ModelSerializer):
    class Meta:
        model = RequestRoomMate
        exclude = ('user', 'image_package', )
    


class PendingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pending
        fields = '__all__'
    
class CreatePendingSerializer(PendingSerializer):
    class Meta:
        model = Pending
        exclude = ('user_want_to_join', 'status')
    

class ContractSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contract
        fields = '__all__'
    
    
class CreateContractSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contract
        exclude = ('requested_person',)
    
    def create(self, validated_data):
        accepted_persons = validated_data.pop('accepted_persons')
        request_room_mate = validated_data.get('request_room_mate')
        contract = Contract.objects.create(**validated_data)
        for accepted_person in accepted_persons:
            pending = request_room_mate.pending_set.filter(user_want_to_join=accepted_person)
            pending.update(status = 1) #  Pending.status_choices[1][0]
            print(pending)
            # pending[0].save()
            contract.accepted_persons.add(accepted_person)
        return contract



class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = '__all__'

class CreateReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        exclude = ('sender_review', 'date_time', )


class ChatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chat
        fields = '__all__'

class CreateChatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chat
        exclude = ('send_time', 'sender_chat',)
    
class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = '__all__'
    


    