from django.db import models
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator,MinValueValidator
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User

class ImagePackage(models.Model):
    id = models.AutoField(primary_key=True)
    def __str__(self):
        return str(self.id)


class Profile(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    rate = models.FloatField(validators=[MinValueValidator(0.0), MaxValueValidator(5.0)])
    number_of_persons_rated = models.PositiveIntegerField(default=0)

    user = models.OneToOneField(
        to=get_user_model(),
        on_delete=models.CASCADE,
    )

    image_package = models.OneToOneField(
        to=ImagePackage,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.user.username


class RequestRoomMate(models.Model):
    request_status_choices = [
        (0, 'active'),
        (1, 'cancled'),
        (2, 'founded'),
    ]

    request_status = models.IntegerField(choices=request_status_choices, default=0)
    content = models.TextField()
    longitude = models.DecimalField(max_digits=9, decimal_places=6)
    latitude = models.DecimalField(max_digits=9, decimal_places=6)
    start_contract_date = models.DateField(blank=True)
    end_contract_date =  models.DateField(blank=True)
    area = models.PositiveIntegerField()
    floor = models.IntegerField()
    number_of_persons_will_be_in_room = models.PositiveIntegerField()

    user = models.ForeignKey(
        to=get_user_model(),
        on_delete=models.CASCADE,
    )

    image_package = models.OneToOneField(
        to=ImagePackage,
        on_delete=models.CASCADE,
    )

class Contract(models.Model):
    contract_status_choices = [
        (0, 'active'),
        (1, 'inactive'),
    ]
    contract_status = models.IntegerField(choices=contract_status_choices, default=0)

    requested_person = models.ForeignKey(
        to=get_user_model (),
        on_delete=models.CASCADE,
        related_name='contracts_created'
    )
    accepted_persons = models.ManyToManyField(
        to=get_user_model (),
        related_name='contracts_accepted',
    )

    request_room_mate = models.OneToOneField(
        to=RequestRoomMate,
        on_delete=models.CASCADE,
    )

    
class Chat(models.Model):
    content = models.TextField()
    send_time = models.IntegerField()   

    sender_chat = models.ForeignKey(
        to=get_user_model (),
        on_delete=models.CASCADE,
        related_name='chat_sended',
    )
    receiver_chat = models.ForeignKey(
        to=get_user_model (),
        on_delete=models.CASCADE,
        related_name='chat_received',
    )


class Review(models.Model):
    rate = models.PositiveIntegerField(validators=[MaxValueValidator(5.0)])
    content = models.TextField()
    date_time = models.DateTimeField()

    sender_review = models.ForeignKey(
        to=get_user_model(),
        on_delete=models.CASCADE,
        related_name='review_sended',
    )
    receiver_review = models.ForeignKey(
        to=get_user_model(),
        on_delete=models.CASCADE,
        related_name='review_received',
    )
    request_room_mate = models.ForeignKey(
        to=RequestRoomMate,
        on_delete=models.CASCADE,
    )


class PictureProfile(models.Model):
    # category_type = choices
    image_package = models.ForeignKey(
        to=ImagePackage,
        on_delete=models.CASCADE,
    )

class ImageFile(models.Model):
    format_type_choices = [
        (0, 'svg'),
        (1, 'jpeg'),
        (2, 'png'),
    ]
    image_size_type_choices = [
        (0, 'small'),
        (1, 'medium'),
        (2, 'big'),
    ]

    url = models.CharField(max_length=400)
    width = models.PositiveIntegerField()
    height = models.PositiveIntegerField()
    format_type = models.IntegerField(choices=format_type_choices)
    image_size_type = models.IntegerField(choices=image_size_type_choices)

    picture_profile = models.ForeignKey(
        to=PictureProfile,
        on_delete=models.CASCADE
    )

class Pending(models.Model):
    status_choices = [
        (0, 'pending'),
        (1, 'failed'),
        (2, 'accepted'),
    ]

    status = models.IntegerField(choices=status_choices)

    user_want_to_join = models.ForeignKey(
        to=get_user_model (),
        on_delete=models.CASCADE,
    )
    request_room_mate = models.ForeignKey(
        to=RequestRoomMate,
        on_delete=models.CASCADE,
    )

class Contact(models.Model):
    owner = models.OneToOneField(
        to=User,
        on_delete=models.CASCADE,
    )
    user = models.ManyToManyField(
        to=User,
        related_name='me_save_on_contact',
    )