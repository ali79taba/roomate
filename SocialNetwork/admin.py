from django.contrib import admin
from SocialNetwork import models


admin.site.register(models.Profile)
admin.site.register(models.RequestRoomMate)
admin.site.register(models.Contract)
admin.site.register(models.Chat)
admin.site.register(models.Review)
admin.site.register(models.Pending)
admin.site.register(models.PictureProfile)
admin.site.register(models.ImageFile)
admin.site.register(models.ImagePackage)
admin.site.register(models.Contact)